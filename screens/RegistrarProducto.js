import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button,Image
  } from "react-native";

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';
import * as ImagePicker from 'expo-image-picker';

const Tab = createBottomTabNavigator();

export default function RegistrarProducto(){

  
    const [image, setImage] = useState('');

    const pickImage = async () => {
       
        let result = await ImagePicker.launchImageLibraryAsync({
          mediaTypes: ImagePicker.MediaTypeOptions.All,
          allowsEditing: true,
          aspect: [4, 3],
          quality: 1,
        });
    
        console.log(result);
    
        if (!result.cancelled) {
          setImage(result.uri);
        }
      };
    
    return(
        <View style={forms.container}> 
          <FontAwesome5.Button
          name='camera'
          onPress={pickImage}
        />
        {image && <Image source={{ uri: image }} style={{ width: 200, height: 200 }} />}
        <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        placeholder='Nombre'
        //onChangeText={onChangeValue}
        />
      
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Precio'
          //onChangeText={onChangeValue}
        />
        
        <TextInput
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            style={forms.editable}
        />
        <Button  title="Registrar"  />
      </View>
      );
    
  }

  const forms = StyleSheet.create({
      container: {
        margin: 10
      },
      alert: {
        margin: 15,
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
      },
      uneditable: {
        padding: 5,
        marginBottom: 10
      },
      editable: {
        padding: 5,
        height: 40,
        borderColor: "#000",
        borderWidth: 1,
        marginBottom: 10,
        backgroundColor: "white",
        borderRadius: 10,
        marginBottom: 10,
        color: "#000"
      }
    });
    
  