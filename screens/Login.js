import React,{ useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity
} from "react-native";
import { RSA } from 'react-native-rsa-native';


function Login(props) {

  
  const [use, setUser] = useState('');
  const [pass, setPass] = useState('');
  const [data, setData] = useState( 
  [
    { id: 1, value: "admin", correo: 'admin@barberyshop.com', pass: '123' },
    { id: 1, value: "user", correo: 'sincere@april.biz', pass: '123' },
    {id: 1, value: "empleado", correo: 'empleado@barberyshop.gmail.com', pass: '1234' }
  ]);
  const [log, setLog] = useState();

  var RSAKey = require('react-native-rsa');
  const bits = 1024;
  const exponent = '10001'; 
  var rsa = new RSAKey();
  var r = rsa.generate(bits, exponent);
  var publicKey = rsa.getPublicString(); 
  var privateKey = rsa.getPrivateString();
  var rsa = new RSAKey();
  rsa.setPublicString(publicKey);

  function Log(){
  
    console.log(data)
    var encrypted = rsa.encrypt(pass);
    let user = data.filter((c) => c.correo === use)
    if(user.length != 0){

      if(user[0].value == 'admin'){
        props.navigation.navigate('Principal')
      }else if(user[0].value == 'user'){
        props.navigation.navigate('PrincipalC')
      }
    
    }else{
      alert('Datos incorrectos')
    }
  }


  return (
    <View style={styles.container}>
      <Text style={styles.Welcome}>BarberyShop</Text>

      <TextInput
        placeholder="Correo"
        underlineColorAndroid="transparent"
        style={styles.TextInputStyleClass}
        value={use}
        onChangeText={setUser}
      />

      <TextInput
        placeholder="Password"
        secureTextEntry={true}
        value={pass}
        onChangeText={setPass}
        underlineColorAndroid="transparent"
        style={styles.TextInputStyleClass}
      />

      <TouchableOpacity style={styles.SubmitButtonStyle} activeOpacity={0.5}>
        <Text style={styles.TextStyle} onPress={() => Log()}> Ingresar</Text>
      </TouchableOpacity>

    </View>
  );
}

export default Login;

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "#fff",
    justifyContent: "center",
    alignItems: "center"
  },

  Welcome: {
    fontSize: 19,
    color: "#000",
    fontWeight: "bold",
    marginBottom: 30,
    letterSpacing: 2
  },

  TextInputStyleClass: {
    height: 50,
    width: "80%",
    padding: 20,
    borderWidth: 1,
    borderColor: "#000",
    borderRadius: 40,
    marginBottom: 10,
    color: "#000"
  },

  SubmitButtonStyle: {
    width: "33%",
    padding: 10,
    backgroundColor: "#65b6a0",
    borderRadius: 50,
    marginTop: 50,
    marginBottom: 30
  },

  TextStyle: {
    color: "#000",
    textAlign: "center",
    letterSpacing: 2
  },

  HyperLink: {
    textDecorationLine: "underline"
  }
});