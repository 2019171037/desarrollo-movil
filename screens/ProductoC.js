import React,{ useEffect, useState, Component } from "react";
import {
  StyleSheet,
  Text,
  View,
  TouchableOpacity,
  Image,
  Alert,
  ScrollView,
  FlatList,
  Button
} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';


export default function ProductosC(props){
  
  const [data, setData] = useState(
    [
      {"id":114,"brand":"covergirl","name":"America crew","price":"10.99","price_sign":null,"currency":null, "image_link":"https://m.media-amazon.com/images/I/61UbGHvMIOL._AC_SX679_.jpg"},
      {"id":113,"brand":"covergirl","name":"Afeitadora","price":"12.99","price_sign":null,"currency":null,  "image_link":"https://m.media-amazon.com/images/I/51bo7xPi7jL._AC_SX679_.jpg"},
      {"id":102,"brand":"covergirl","name":"Tinte barba","price":"12.99","price_sign":null,"currency":null,"image_link":"https://images-na.ssl-images-amazon.com/images/I/81VhVac7eOL.__AC_SX300_SY300_QL70_ML2_.jpg"},
      {"id":1048,"brand":"colourpop","name":"Lippie Pencil","price":"5.0","price_sign":"$","currency":"CAD","image_link":"https://cdn.shopify.com/s/files/1/1338/0845/collections/lippie-pencil_grande.jpg?v=1512588769"}
    ]);

  function onDelete(id){
    setData(data.filter((c) => c.id !== id));
  }

  useEffect(()=>{
    postData()
  },[]);

  return (
    <View style={styles.container}>
      <FlatList style={styles.list}
        contentContainerStyle={styles.listContainer}
        data={data}
        horizontal={false}
        numColumns={2}
        keyExtractor= {(item) => {
          return item.id;
        }}
        ItemSeparatorComponent={() => {
          return (
            <View style={styles.separator}/>
          )
        }}
        renderItem={(post) => {
          const item = post.item;
          return (
            <View style={styles.card}>
             
             <View style={styles.cardHeader}>
                <View>
                  <Text style={styles.title}>{item.name}</Text>
                  <Text style={styles.price}>{item.price}</Text>
                </View>
              </View>

              <Image style={styles.cardImage} source={{uri:item.image_link}}/>
              
            </View>
          )
        }}/>
    </View>
  );
}

const styles = StyleSheet.create({
container:{
  flex:1,
  marginTop:20,
},
list: {
  paddingHorizontal: 5,
  backgroundColor:"#E6E6E6",
},
listContainer:{
  alignItems:'center'
},
separator: {
  marginTop: 10,
},
/******** card **************/
card:{
  shadowColor: '#00000021',
  shadowOffset: {
    width: 2
  },
  shadowOpacity: 0.5,
  shadowRadius: 4,
  marginVertical: 8,
  backgroundColor:"white",
  flexBasis: '47%',
  marginHorizontal: 5,
},
cardHeader: {
  paddingVertical: 17,
  paddingHorizontal: 16,
  borderTopLeftRadius: 1,
  borderTopRightRadius: 1,
  flexDirection: 'row',
  justifyContent: 'space-between',
},
cardContent: {
  paddingVertical: 12.5,
  paddingHorizontal: 16,
},
cardFooter:{
  flexDirection: 'row',
  justifyContent: 'space-between',
  paddingTop: 12.5,
  paddingBottom: 25,
  paddingHorizontal: 16,
  borderBottomLeftRadius: 1,
  borderBottomRightRadius: 1,
},
cardImage:{
  flex: 1,
  height: 150,
  width: null,
},
/******** card components **************/
title:{
  fontSize:18,
  flex:1,
},
price:{
  fontSize:16,
  color: "green",
  marginTop: 5
},
buyNow:{
  color: "purple",
},
icon: {
  width:25,
  height:25,
},
/******** social bar ******************/
socialBarContainer: {
  justifyContent: 'center',
  alignItems: 'center',
  flexDirection: 'row',
  flex: 1
},
socialBarSection: {
  justifyContent: 'center',
  flexDirection: 'row',
  flex: 1,
},
socialBarlabel: {
  marginLeft: 8,
  alignSelf: 'flex-end',
  justifyContent: 'center',
},
socialBarButton:{
  flexDirection: 'row',
  justifyContent: 'center',
  alignItems: 'center',
}
});  