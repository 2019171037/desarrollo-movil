import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";

import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';


const Tab = createBottomTabNavigator();

export default function RegistrarUsuario(){
    return(
        <View style={forms.container}> 
        <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        placeholder='Username'
        //onChangeText={onChangeValue}
        />
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Nombre'
          //onChangeText={onChangeValue}
        />
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Telefono'
          //onChangeText={onChangeValue}
        />
        <TextInput
          style={forms.editable}
          editable
          maxLength={20}
          placeholder='Correo'
         // onChangeText={(value) => onChangeDesc(value)}
        />
        
        <TextInput
            placeholder="Password"
            secureTextEntry={true}
            underlineColorAndroid="transparent"
            style={forms.editable}
        />
        <Button  title="Registrar"  />
      </View>
      );
    
  }
  
  const forms = StyleSheet.create({
      container: {
        margin: 10
      },
      alert: {
        margin: 15,
        fontSize: 20,
        fontWeight: "bold",
        textAlign: "center"
      },
      uneditable: {
        padding: 5,
        marginBottom: 10
      },
      editable: {
        padding: 5,
        height: 40,
        borderColor: "#000",
        borderWidth: 1,
        marginBottom: 10,
        backgroundColor: "white",
        borderRadius: 10,
        marginBottom: 10,
        color: "#000"
      }
    });
    
  