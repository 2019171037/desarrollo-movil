import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";


export default function EditarCiA(props){

    
    const [id, setId] = useState('');
    const [username, setUsername] = useState('');
    const [horario, setHorario] = useState('');
    const [fecha, setfecha] = useState('');
    const [servicio, setServicio] = useState('');

    useEffect(() => {
        setId(props.route.params.idUser);
        setUsername(props.route.params.usuario);
        setHorario(props.route.params.horario);
        setfecha(props.route.params.fecha);
        setServicio(props.route.params.servicio);

        
    },[])

    return ( 
    <View style={forms.container}>
      <Text style={forms.uneditable}>Usuario: {username}  </Text>
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={horario}
        //onChangeText={onChangeValue}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={fecha}
        //onChangeText={onChangeValue}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={servicio}
       // onChangeText={(value) => onChangeDesc(value)}
      />
      <Button  title="Modificar"  />
    </View>
    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  
