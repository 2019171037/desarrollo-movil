import React,{ useEffect, useState, Component } from "react";
import {
    StyleSheet,
    Text,
    View,
    TouchableOpacity,
    Image,
    Alert,
    ScrollView,
    FlatList,
    Button
  } from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';


const Tab = createBottomTabNavigator();

export default function CitasC(props){
  const [data, setData] = useState( 
    [
      { id: 0, usuario: 'Leanne Graham', horario: '10:30', servicio: 'Corte de cabello', fecha: '10-04-2022'},
    ]);

    function onDelete(id){
      setData(data.filter((c) => c.id !== id));
    }

    return(
      <ScrollView>
        <View style={lists.fixToButon}>
        <FontAwesome5.Button
              onPress={() => props.navigation.navigate("Registrar usuario")}
              name='plus'
              color="black"
              backgroundColor="white"
              style={buttons.primary}
             />
        </View>
        

        {data.map((user) => (
          <View style={lists.list} key={user.id}>
            <View style={lists.fixToText}>
              <View>
                <Text>Usuario: {user.usuario}</Text>
                <Text>Horario: {user.horario}</Text>
                <Text>servicio: {user.servicio}</Text>
              </View>
            </View>
            <FontAwesome5
                  onPress={() => props.navigation.navigate("Editar cita", { id: user.id, usuario: user.usuario, horario: user.horario, servicio: user.servicio, fecha: user.fecha })}
                  name='edit'
                  backgroundColor='blue'
                  size={25}
                />
            <FontAwesome5.Button
                  onPress={() => onDelete(user.id)} 
                  name='trash'
                  backgroundColor="red"
                  size={25}
                  />
            </View>
        ))}
      </ScrollView>
    
    );

}

const styles = new StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#ffff',
      alignItems: 'center',
      justifyContent: 'center',
  },
  Titulo: {
    fontSize: 19,
    color: "#000",
    fontWeight: "bold",
    marginBottom: 30,
    letterSpacing: 2
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "50%"
  },
  fixToButon: {
    width: "20%"

  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});

const forms = StyleSheet.create({
  container: {
    margin: 10
  },
  alert: {
    margin: 15,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  uneditable: {
    padding: 5,
    marginBottom: 10
  },
  editable: {
    padding: 5,
    height: 5,
    width: 5,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
    backgroundColor: "white"
  }
});

const buttons = StyleSheet.create({
  primary: {
    flex: 1,
    height: 40,
    width: 20,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    marginRight: 5
  }
});

const lists = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "left",
    marginHorizontal: 8,
    marginTop: 10,
    backgroundColor: "#ffff"
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap",
  },
  fixToText: {
    width: "60%"
  },
  separator: {
    marginVertical: 4,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});