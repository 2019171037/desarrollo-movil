import React,{ useEffect, useState } from "react";
import {
    View,
    Text,
    StyleSheet,
    TextInput,
    TouchableOpacity, ScrollView,Button
  } from "react-native";


export default function Detalle(props){

    
    const [id, setId] = useState('');
    const [username, setUsername] = useState('');
    const [name, setName] = useState('');
    const [phone, setPhone] = useState('');
    const [email, setEmail] = useState('');

    useEffect(() => {
        setId(props.route.params.idUser);
        setUsername(props.route.params.username);
        setName(props.route.params.name);
        setPhone(props.route.params.correo);
        setEmail(props.route.params.tel);

        
    },[])

    return ( 
    <View style={forms.container}>
      <Text style={forms.uneditable}>Usuario: {username}  </Text>
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={name}
        //onChangeText={onChangeValue}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={phone}
        //onChangeText={onChangeValue}
      />
      <TextInput
        style={forms.editable}
        editable
        maxLength={20}
        value={email}
       // onChangeText={(value) => onChangeDesc(value)}
      />
      <Button  title="Modificar"  />
    </View>
    );
  
}

const forms = StyleSheet.create({
    container: {
      margin: 10
    },
    alert: {
      margin: 15,
      fontSize: 20,
      fontWeight: "bold",
      textAlign: "center"
    },
    uneditable: {
      padding: 5,
      marginBottom: 10
    },
    editable: {
      padding: 5,
      height: 40,
      borderColor: "#000",
      borderWidth: 1,
      marginBottom: 10,
      backgroundColor: "white",
      borderRadius: 10,
      marginBottom: 10,
      color: "#000"
    }
  });
  
