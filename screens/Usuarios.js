import React,{ useEffect, useState } from "react";
import {
  View,
  Text,
  StyleSheet,
  TextInput,
  TouchableOpacity, ScrollView,Button
} from "react-native";
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

export default function Usuarios(props){

  const [data, setData] = useState([]);

  const [id, setId] = useState('');
  const [username, setUsername] = useState('');
  const [name, setName] = useState('');
  const [phone, setPhone] = useState('');
  const [email, setEmail] = useState('');

  function postData() {
    const response =  fetch('https://jsonplaceholder.typicode.com/users', {
    method: 'GET',
    headers: {'Content-Type': 'application/json'}
    }).then((response) => response.json())
    .then((responseJson) => {
      setData(responseJson);
    })
    .catch((error) => {
      console.error(error);
    });
  }

  function onDelete(id){
    setData(data.filter((c) => c.id !== id));
  }


  useEffect(()=>{
    postData()
  },[]);
    return(
      <ScrollView>
        <View style={lists.fixToButon}>
        <FontAwesome5.Button
              onPress={() => props.navigation.navigate("Registrar usuario")}
              name='plus'
              color="black"
              backgroundColor="white"
              style={buttons.primary}
             />

        </View>
        

        {data.map((user) => (
          <View style={lists.list} key={user.id}>
            <View style={lists.fixToText}>
              <View>
                <Text>Usuario: {user.username}</Text>
                <Text>Nombre: {user.name}</Text>
              </View>
            </View>
            <FontAwesome5
                  onPress={() => props.navigation.navigate("Editar", { idUser: user.id, username : user.username, name: user.name, correo: user.email, tel: user.phone })}
                  name='edit'
                  backgroundColor='blue'
                  size={25}
                />
            <FontAwesome5
                  onPress={() => onDelete(user.id)} 
                  name='trash'
                  backgroundColor="red"
                  size={25}
                  />
            </View>
        ))}
      </ScrollView>
    
    );

}

const styles = new StyleSheet.create({
  container: {
      flex: 1,
      backgroundColor: '#ffff',
      alignItems: 'center',
      justifyContent: 'center',
  },
  Titulo: {
    fontSize: 19,
    color: "#000",
    fontWeight: "bold",
    marginBottom: 30,
    letterSpacing: 2
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "50%"
  },
  fixToButon: {
    width: "20%"

  },
  separator: {
    marginVertical: 8,
    borderBottomColor: "#737373",
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});

const forms = StyleSheet.create({
  container: {
    margin: 10
  },
  alert: {
    margin: 15,
    fontSize: 20,
    fontWeight: "bold",
    textAlign: "center"
  },
  uneditable: {
    padding: 5,
    marginBottom: 10
  },
  editable: {
    padding: 5,
    height: 5,
    width: 5,
    borderColor: "gray",
    borderWidth: 1,
    marginBottom: 10,
    backgroundColor: "white"
  }
});

const buttons = StyleSheet.create({
  primary: {
    flex: 1,
    height: 40,
    width: 20,
    justifyContent: "center",
    alignItems: "center",
    marginLeft: 5,
    marginRight: 5
  }
});

const lists = StyleSheet.create({
  container: {
    flex: 1,
    // justifyContent: "left",
    marginHorizontal: 8,
    marginTop: 10,
  },
  title: {
    textAlign: "center",
    marginVertical: 8
  },
  list: {
    flex: 1,
    justifyContent: "space-between",
    flexDirection: "row",
    padding: 10,
    flexWrap: "wrap"
  },
  fixToText: {
    width: "60%"
  },
  separator: {
    marginVertical: 4,
    borderBottomWidth: StyleSheet.hairlineWidth
  },
  scrollView: {
    marginHorizontal: 5
  }
});