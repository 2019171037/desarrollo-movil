import React,{ useEffect, useState, Component } from "react";
import { Text, View } from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

import CitasC from './CitasC';
import ProductosC from './ProductoC';

export default function PrincipalC(props){
    return(
        <Tab.Navigator
        screenOptions={({route}) => ({
            tabBarIcon :({color}) => {
                let icono;
                if(route.name == 'Citas'){
                    icono = 'calendar-alt';
                } else if(route.name == 'Usuarios'){
                    icono = 'user-alt';
                } else if(route.name == 'Productos'){
                    icono = 'pump-soap';
                }

                return <FontAwesome5 name={icono} size={20} color={color}/>
            } 
        })
    }
        tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'gray',
            showLabel: false,
        
        }}
        >
            <Tab.Screen
            name='Citas cliente'
            component={CitasC}
            options={{title: 'Citas'}}
            />
            <Tab.Screen
            name='Productos cliente'
            component={ProductosC}
            options={{title: 'Productos'}}
            />
        </Tab.Navigator>
    );

}