import React,{ useEffect, useState, Component } from "react";
import { Text, View,Form} from 'react-native';
import {createBottomTabNavigator} from '@react-navigation/bottom-tabs';
import {FontAwesome5} from '@expo/vector-icons';

const Tab = createBottomTabNavigator();

import Citas from './Citas';
import Usuarios from './Usuarios';
import Productos from './Productos';

export default function Principal(props){
    return(
        <Tab.Navigator
        screenOptions={({route}) => ({
            tabBarIcon :({color}) => {
                let icono;
                if(route.name == 'Citas'){
                    icono = 'calendar-alt';
                } else if(route.name == 'Usuarios'){
                    icono = 'user-alt';
                } else if(route.name == 'Productos'){
                    icono = 'pump-soap';
                }

                return <FontAwesome5 name={icono} size={20} color={color}/>
            } 
        })
    }
        tabBarOptions={{
            activeTintColor: 'blue',
            inactiveTintColor: 'gray',
            showLabel: false,
        
        }}
        >
            <Tab.Screen
            name='Citas'
            component={Citas}
            />
            <Tab.Screen
            name='Usuarios'
            component={Usuarios}
            options={{title: 'Usuarios'}}
            />
            <Tab.Screen
            name='Productos'
            component={Productos}
            options={{title: 'Productos'}}
            />
        </Tab.Navigator>
    );

}