import React from 'react';
import { NavigationContainer } from '@react-navigation/native';
import { createStackNavigator } from '@react-navigation/stack';


import Login from './screens/Login';
import Productos from './screens/Productos';
import Citas from './screens/Citas';
import Usuarios from './screens/Usuarios';
import Principal from './screens/Principal';
import DetalleP from './screens/DetalleP';
import RegistrarUsuario from './screens/RegistarUsuario';
import EditarCC from './screens/EditarCC';
import EditarCiA from './screens/EditarCiA';
import RegistrarProducto from './screens/RegistrarProducto';
import PrincipalC from './screens/PrincipalC';
import ProductosC from './screens/ProductoC';

import 'react-native-gesture-handler';


const Stack = createStackNavigator();

export default function App() {
  return (
    <NavigationContainer>
      <Stack.Navigator
      screenOptions={{
        headerStyle : {
          backgroundColor: '#FFF'
        },
        headerTintColor: '#3086F7'
      }}
      >
        {/* 
        Representamos cada screen de RN
        como un Stack.Screen
        */}
        <Stack.Screen
          name='Login'
          component={Login}
          options = {{title : ''}}
        />
         <Stack.Screen
          name='Productos'
          component={Productos}
          options = {{title : 'Productos'}}
        />
        <Stack.Screen
         name='Registrar producto'
         component={RegistrarProducto}
         options = {{title : 'Productos'}}
       />
        <Stack.Screen
         name='Citas'
         component={Citas}
         options = {{title : 'Citas'}}
       />
        <Stack.Screen
          name='Usuarios'
          component={Usuarios}
          options = {{title : 'Usuarios'}}
        />
        <Stack.Screen
          name='Registrar usuario'
          component={RegistrarUsuario}
          options = {{title : 'Usuarios'}}
        />
        <Stack.Screen
        name='Principal'
        component={Principal}
        options = {{title : 'BarberyShop'}}
        />
        
        <Stack.Screen
        name='PrincipalC'
        component={PrincipalC}
        options = {{title : 'BarberyShop'}}
        />
      
        <Stack.Screen
          name='Editar'
          component={DetalleP}
          options = {{title : 'BarberyShop'}}
        />
        <Stack.Screen
          name='Editar cita admin'
          component={EditarCC}
          options = {{title : 'BarberyShop'}}
        /> 
         <Stack.Screen
          name='ProductosC'
          component={ProductosC}
          options = {{title : 'BarberyShop'}}
        /> 
        
        <Stack.Screen
        name='Editar cita'
        component={EditarCiA}
        options = {{title : 'BarberyShop'}}
      />
  
      </Stack.Navigator>
    </NavigationContainer>
  );
}
